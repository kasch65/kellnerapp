console.log('Custom service worker loading...')

let notificationUrl = ''

self.addEventListener('push', event => {
	console.log('Push received: ', event)
	const _data = event.data ? JSON.parse(event.data.text()) : {}
	console.log('Push data received: ', _data)
	self.clients.matchAll().then(all => all.forEach(client => {
		console.debug('Forwarding push message to: ', client)
		// Event to App.js
		client.postMessage(_data)
	}
	))
	if (_data.kind === 'bestellungen') {
		if (_data.action === 'post') {
			notificationUrl = '/#bestellungen'
			const bemerkungen = _data.body.bemerkungen ? `, Bemerkungen: ${_data.body.bemerkungen}` : ''
			event.waitUntil(
				self.registration.showNotification(_data.body.name, {
					body: `Tisch: ${_data.body.tisch}, Bedienung: ${_data.body.bedienung}, Bestellung: ${_data.body.name}${bemerkungen}`,
					tag: _data.body.name,
					ttl: 30000,
					//icon: _data.icon,
					//icon: 'https://cdn3.iconfinder.com/data/icons/happy-x-mas/501/santa15-128.png',
					icon: 'baseline_speaker_notes_black_18dp.png',
					//badge: 'https://cdn3.iconfinder.com/data/icons/happy-x-mas/501/santa15-128.png',
					vibrate: [100, 100, 100],
				})
			)
		}
		if (_data.action === 'patch') {
			if (_data.body.bereit) {
				notificationUrl = '/#bestellungen'
				const bemerkungen = _data.details.bemerkungen ? `, Bemerkungen: ${_data.details.bemerkungen}` : ''
				event.waitUntil(
					self.registration.showNotification('Bereit: ' + _data.details.name, {
						body: `Tisch: ${_data.details.tisch}, Bedienung: ${_data.details.bedienung}${bemerkungen}`,
						tag: 'Bereit ' + _data.details.name,
						ttl: 300000,
						//icon: _data.icon,
						//icon: 'https://cdn3.iconfinder.com/data/icons/happy-x-mas/501/santa15-128.png',
						icon: 'baseline_fastfood_black_18dp.png',
						//badge: 'https://cdn3.iconfinder.com/data/icons/happy-x-mas/501/santa15-128.png',
						vibrate: [100, 100, 100],
					})
				)
			}
		}
	}
})

self.addEventListener('notificationclick', event => {
	event.notification.close()

	event.waitUntil(
		clients.matchAll({
			type: 'window'
		})
			.then(() => {
				if (clients.openWindow) {
					// TODO improve with https://developers.google.com/web/fundamentals/push-notifications/common-notification-patterns#focus_an_existing_window
					return clients.openWindow(notificationUrl)
				}
			})
	)
})