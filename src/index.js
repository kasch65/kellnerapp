import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { Provider, mergeThemes, themes } from '@fluentui/react'
import App from './components/App'
import * as serviceWorker from './serviceWorker'

const templateTheme = themes.teamsDark

const appTheme = {
	componentVariables: {
		Label: () => ({
			height: '2rem'
		})
	}
}

//ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(
	<Provider theme={mergeThemes(templateTheme, appTheme)}>
		<App />
	</Provider>,
	document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register()
