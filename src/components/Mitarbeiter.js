import React, { useState } from 'react'
import Table from './Table'
import { Checkbox, Input, Button, Flex } from '@fluentui/react'

const Mitarbeiter = ({ bedienungen, saveMitarbeiterChanges }) => {
	const [mitarbeiterState, setMitarbeiterState] = useState(JSON.parse(JSON.stringify(bedienungen)))
	const [changes, setChanges] = useState(new Map())
	const [newMas, setNewMas] = useState(new Map())
	const [expanded, setExpanded] = useState(false)

	const clickStyle = {
		cursor: 'pointer'
	}

	const _saveChange = (name, model, value) => {
		console.debug(name, model, value)
		if (model.id) {
			setChanges(old => {
				old.set(
					`ma_${model.id}_update_${name}`,
					{
						id: model.id,
						action: 'update',
						field: name,
						value: value,
						originalValue: model[name]
					}
				)
				setMitarbeiterState(oldArray => [...oldArray])
				return new Map(old)
			})
		}
		else {
			setNewMas(oldArray => {
				const newMa = oldArray.get(`ma_${model.tmpId}_create`)
				if (newMa) {
					newMa.content[name] = value
					return new Map(oldArray)
				}
				else {
					return oldArray
				}
			})
		}
	}

	const _delete = m => {
		console.log('Delete: ', m)
		if (m.id) {
			setMitarbeiterState(old => old.filter(ma => ma.id !== m.id))
			setChanges(old => {
				old.set(
					`ma_${m.id}_delete`,
					{
						id: m.id,
						action: 'delete'
					}
				)
				return new Map(old)
			})
		}
		else {
			setMitarbeiterState(old => old.filter(ma => ma.tmpId !== m.tmpId))
			setNewMas(old => {
				old.delete(`ma_${m.tmpId}_create`)
				return new Map(old)
			})
		}
	}


	const _tableHeaders = ['Name', 'Bedienung', 'Kassierer', 'Tresen', 'Kueche', 'Buchhaltung', 'Chef', 'PIN', 'löschen']

	const _tableContent = mitarbeiterState.map(m =>
		[
			['Name', <Input key="Name" defaultValue={m.name} onChange={ev => _saveChange('name', m, ev.target.value)} />, m.name],
			['Kellner', <Checkbox key="Kellner" checked={m.rolleBedienung} onChange={(state) => _saveChange('rolleBedienung', m, state)} />],
			['Kassierer', <Checkbox key="Kassierer" checked={m.rolleKassierer} onChange={(state) => _saveChange('rolleKassierer', m, state)} />],
			['Tresen', <Checkbox key="Tresen" checked={m.rolleTresen} onChange={(state) => _saveChange('rolleTresen', m, state)} />],
			['Kueche', <Checkbox key="Kueche" checked={m.rolleKueche} onChange={(state) => _saveChange('rolleKueche', m, state)} />],
			['Buchhaltung', <Checkbox key="Buchhaltung" checked={m.rolleBuchhaltung} onChange={(state) => _saveChange('rolleBuchhaltung', m, state)} />],
			['Chef', <Checkbox key="Chef" checked={m.rolleChef} onChange={(state) => _saveChange('rolleChef', m, state)} />],
			['PIN', <Input key="PIN" defaultValue={m.pin} onChange={ev => _saveChange('pin', m, ev.target.value)} />],
			['löschen', <Button key="storno" aria-label="Löschen" icon="close" iconOnly title="Löschen" onClick={() => _delete(m)} />]
		]
	)

	return (
		<div id="mitarbeiter">
			<h1 onClick={() => setExpanded(oldValue => !oldValue)} style={clickStyle}>Mitarbeiterverwaltung <Button circular icon={expanded ? 'chevron-down' : 'icon-chevron-end'} iconOnly loader="expand" /></h1>
			{
				expanded &&
				<>
					<Table tableHeaders={_tableHeaders} tableContent={_tableContent} getRowKey={row => row[0][2]} />
					<Button content="Neu" icon="add" iconPosition="after" onClick={() => {
						const maNeu = {
							tmpId: Math.random(),
							rolleBedienung: false,
							rolleKassierer: false,
							rolleTresen: false,
							rolleKueche: false,
							rolleBuchhaltung: false,
							rolleChef: false,
							pin: Math.floor(Math.random() * 10000)
						}
						setMitarbeiterState(old => old.concat(maNeu))
						setNewMas(old => {
							old.set(
								`ma_${maNeu.tmpId}_create`,
								{
									action: 'create',
									content: maNeu
								}
							)
							return new Map(old)
						})
					}} />
					<Flex gap="gap.small" wrap>
						<Button content="Speichern" icon="accept" iconPosition="after" disabled={changes.size + newMas.size < 1} onClick={() => {
							saveMitarbeiterChanges(Array.from(changes.values()).filter(e => e.value !== e.originalValue || e.action === 'delete'), Array.from(newMas.values()))
							setChanges(new Map())
							setNewMas(new Map())
						}} />
						<Button content="Reset" icon="undo" iconPosition="after" disabled={changes.size + newMas.size < 1} onClick={() => {
							// Force tabe to be rendered frechly
							setMitarbeiterState([])
							setTimeout(() => setMitarbeiterState(JSON.parse(JSON.stringify(bedienungen))), 10)
							setChanges(new Map())
							setNewMas(new Map())
						}} />
					</Flex>
				</>
			}
		</div>
	)
}

export default Mitarbeiter