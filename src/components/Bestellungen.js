import React, { useState } from 'react'
import Table from './Table'
import { Button, Checkbox, Form, Flex } from '@fluentui/react'

const Bestellungen = ({ bestellungen, bedienung, tisch, setBestellungen, setBereit, setRechnung, onDelete, rollen }) => {
	const [alleTische, setAlleTische] = useState(rollen.rolleTresen || rollen.rolleKueche || rollen.rolleChef)
	const [alleBedienungen, setAlleBedienungen] = useState(rollen.rolleKassierer || rollen.rolleTresen || rollen.rolleKueche || rollen.rolleChef)
	const [expanded, setExpanded] = useState(true)

	const formStyle = {
		display: 'block'
	}

	const clickStyle = {
		cursor: 'pointer'
	}

	const _tableHeaders = ['Bedienung', 'Tisch', 'Produkt', 'Bemerkungen', 'Preis', 'bereit', 'Rechnung', 'Storno']

	const _tableContent = bestellungen.filter((b => b.tisch === tisch || alleTische)).filter((b => b.bedienung === bedienung || alleBedienungen)).map((b) =>
		[
			['bedienung', b.bedienung, b.bestelldatum],
			['tisch', b.tisch],
			['name', b.name],
			['bm', b.bemerkungen],
			['preis', b.preis.toFixed(2)],
			['bereit', <Checkbox key="bereit" checked={b.bereit} onClick={() => setBereit(!b.bereit, b)} />],
			['rechnung', <Checkbox key="rechnung" checked={b.rechnung} onClick={() => {
				setRechnung(!b.rechnung, b)
				setAlleBedienungen(true)
			}} />],
			['storno', <Button key="storno" aria-label="Löschen" icon="close" iconOnly title="Löschen" onClick={() => onDelete(b)} />]
		]
	)

	return (
		<>
			{
				bestellungen.length > 0 &&
				<Form id="bestellungen" style={formStyle}>
					<h1 onClick={() => setExpanded(oldValue => !oldValue)} style={clickStyle}>Bestellungen <Button circular icon={expanded ? 'chevron-down' : 'icon-chevron-end'} iconOnly loader="expand" /></h1>
					{
						expanded &&
						<>
							<Flex gap="gap.small" wrap>
								<Checkbox label="Alle Bedienungen" checked={alleBedienungen} onClick={() => setAlleBedienungen(oldValue => !oldValue)} />
								<Checkbox label="Alle Tische" checked={alleTische} onClick={() => setAlleTische(oldValue => !oldValue)} />
								<Button primary icon="bullets" iconPosition="after" content="Alles zusammen" aria-label="Alles zusammen" onClick={() => {
									bestellungen.filter(b => b.tisch === tisch || alleTische).filter(b => b.bedienung === bedienung || alleBedienungen).map((b) => b.rechnung = true)
									const clone = [...bestellungen]
									setBestellungen(clone)
								}} />
							</Flex>
							<Table tableHeaders={_tableHeaders} tableContent={_tableContent} getRowKey={row => row[0][2]} />
						</>
					}
				</Form>
			}
		</>
	)

}

export default Bestellungen
