import React, { useState } from 'react'
import { Icon, Grid, Dialog, TextArea, Form } from '@fluentui/react'
import Breadcrumbs from './Breadcrumbs'

const Karte = ({ karte, setProdukt }) => {
	const _getScreenWidthEm = () => window.innerWidth / parseFloat(
		getComputedStyle(
			document.querySelector('body')
		)['font-size']
	)

	const _getColumns = () => Math.floor(_getScreenWidthEm() / 10)

	const [breadcrumbs, setBreadcrumbs] = useState([karte])
	const [kategorie, setKategorie] = useState(karte)
	// Dialog
	const [detailsOpen, setDetailsOpen] = useState(false)
	const [detailsProduct, setDetailsProdukt] = useState(null)
	const [bemerkungen, setBemerkungen] = useState('')
	// Grid layout
	const [columns, setColumns] = useState(_getColumns())

	const cardStyle = {
		boxSizing: 'content-box',
		//border: '1px solid black',
		textAlign: 'center',
		cursor: 'pointer'
	}

	const kategorieCardStyle = {
		height: '6rem',
		lineHeight: '6rem'
	}

	const produktCardStyle = {
		position: 'relative',
		padding: '1rem .05rem'
	}

	const detailsButtonStyle = {
		position: 'absolute',
		right: '0',
		bottom: '0',
		padding: '.4rem',
		backgroundColor: '#ffffff11'
	}

	window.onresize = () => setColumns(_getColumns())

	let mousetimer

	const _palettes =
		[
			[
				'#582A72',
				'#7D499A',
				'#6A3886',
				'#471D5D',
				'#361349',
				'#882D61',
				'#B9538C',
				'#A13F76',
				'#701F4D',
				'#58133A',
				'#403075',
				'#63519F',
				'#513F8A',
				'#312360',
				'#24174B'
			],
			[
				'#5C4285',
				'#8E69C8',
				'#7252A3',
				'#3E2A5D',
				'#281A3D',
				'#7B397F',
				'#BD5EC3',
				'#97489D',
				'#56245A',
				'#38163A',
				'#464C87',
				'#6F77CA',
				'#575FA6',
				'#2E325F',
				'#1C1F3E'
			],
			[
				'#1D2945',
				'#181F2F',
				'#272F40',
				'#193065',
				'#344D89',
				'#675C25',
				'#463F21',
				'#605936',
				'#978118',
				'#CCB33E',
				'#674425',
				'#463221',
				'#604936',
				'#975318',
				'#CC803E',
				'#675125',
				'#463921',
				'#605236',
				'#976D18',
				'#CC9D3E'
			],
			[
				'#53100A',
				'#5F0C0C',
				'#222D29',
				'#222D29',
				'#2E2922',
				'#2E2922',
				'#222D42',
				'#643816',
				'#4E4A25',
				'#841C1D',
				'#4E3240',
				'#8C1621',
				'#4E4A41',
				'#4E4A41',
				'#901833',
				'#901833',
				'#4E4A4B',
				'#5B533C',
				'#5B3A57',
				'#5B3A57',
				'#785223',
				'#785225',
				'#6B493F',
				'#6B494A',
				'#6B494A',
				'#A82837',
				'#A8283F',
				'#C1143C',
				'#544E72',
				'#544E72',
				'#844D46',
				'#844D46',
				'#977020',
				'#9C5D32',
				'#896E3F',
				'#B0523D',
				'#896D55',
				'#6D776F',
				'#896D60',
				'#B8633C',
				'#846670',
				'#7F746E',
				'#956275',
				'#658190',
				'#DE505C',
				'#C27561',
				'#A78D68',
				'#A78D6C',
				'#808E97',
				'#808E97',
				'#808E99',
				'#808E99',
				'#D37E62',
				'#608EC9',
				'#BB9974',
				'#C6A186',
				'#C6A186'
			]
		]

	const _palette = _palettes[1]

	const _calcColor = (style, name) => {
		const nameHash = name.split('').reduce((a, b) => { a = ((a << 5) - a) + b.charCodeAt(0); return a & a }, 0)
		const absHash = Math.abs(nameHash)
		const colorIndex = absHash % (_palette.length - 1)
		const color = _palette[colorIndex]
		//console.debug('_calcColor', name, nameHash, absHash, colorIndex, color)
		const styleClone = JSON.parse(JSON.stringify(style))
		styleClone.backgroundColor = color
		return styleClone
	}

	let moveCount

	const _executeMouseDown = (p, ev) => {
		ev.persist()
		console.debug('Product.mouseDown', ev)
		ev.stopPropagation()
		if (ev.button === 2) {
			console.debug('Ignoriere Rechtsklick')
			return
		}
		if (mousetimer) {
			console.debug('Event schon berücksichtigt')
			return
		}
		moveCount = 0
		mousetimer = setTimeout(() => {
			console.debug('Timer abgelaufen')
			mousetimer = null
			// Use dialog
			if (moveCount) {
				console.debug('Verwackelt')
				return
			}
			setDetailsProdukt(p)
			setDetailsOpen(true)
		}, 1000)
		console.debug('Timer gestartet')
	}

	const _executeMouseUp = (p, ev) => {
		ev.persist()
		console.debug('Product.mouseUp', ev)
		ev.stopPropagation()
		if (ev.button === 2) {
			console.debug('Ignoriere Rechtsklick')
			return
		}
		if (!mousetimer) {
			console.debug('Event schon berücksichtigt')
			return
		}
		if (mousetimer) {
			clearTimeout(mousetimer)
			console.debug('Timer gestoppt')
			mousetimer = null
			// Losgelassen bevor Timer abgelaufen
			delete p.bemerkungen
			if (moveCount) {
				console.debug('Verwackelt')
				return
			}
			setProdukt(p)
		}
	}

	const _ignoreInteraction = (msg, ev) => {
		ev.persist()
		console.debug('Ignoring ' + msg, ev)
		ev.stopPropagation()
	}

	const _executeMouseMove = () => {
		++moveCount
	}

	const _breadcrumbs = <Breadcrumbs breadcrumbs={breadcrumbs} setBreadcrumbs={(k, breadcrumbs) => {
		console.log(k, breadcrumbs)
		setKategorie(k)
		setBreadcrumbs(breadcrumbs)
	}} cardStyle={cardStyle} calcColor={_calcColor} />

	const _kategorien = kategorie.unterkategorien ? kategorie.unterkategorien.map((k, index) =>
		<div key={k.name} className="card kategorie" style={_calcColor({ ...cardStyle, ...kategorieCardStyle }, k.name)} value={index}
			onClick={() => {
				setBreadcrumbs(oldArray => oldArray.concat(k))
				setKategorie(k)
			}}>{k.name} &gt;</div>)
		: []

	const _produkte = kategorie.produkte ? kategorie.produkte.map((p, index) =>
		<div key={p.name} className="card produkt" style={_calcColor({ ...cardStyle, ...produktCardStyle }, p.name)} value={index}
			onMouseDown={ev => _executeMouseDown(p, ev)}
			onMouseUp={ev => _executeMouseUp(p, ev)}
			onTouchStart={ev => _executeMouseDown(p, ev)}
			onTouchEnd={ev => _executeMouseUp(p, ev)}
			onTouchMove={ev => _executeMouseMove(p, ev)}
			onClick={ev => _ignoreInteraction('Product.onClick', ev) }
		>
			{p.name}<br />{p.preis.toFixed(2)}€
			<div style={detailsButtonStyle}
				onClick={ev => {
					ev.stopPropagation()
					ev.persist()
					console.debug('Details.onClick', ev)
					// Use dialog
					setDetailsProdukt(p)
					setDetailsOpen(true)
				}}
				onMouseDown={ev => _ignoreInteraction('Details.onMouseDown', ev) }
				onTouchStart={ev => _ignoreInteraction('Details.onTouchStart', ev) }
				onMouseUp={ev => _ignoreInteraction('Details.onMouseUp', ev) }
				onTouchEnd={ev => _ignoreInteraction('Details.onTouchEnd', ev) }
			><Icon name="more" /></div>
		</div>)
		: []

	return (
		<div id="karte">
			<Grid id="breadcrumbs" columns={columns} content={_breadcrumbs} />
			{
				_kategorien.length > 0 &&
				<div>
					<h2>Unterkategorien</h2>
					<Grid id="kategorien" columns={columns} content={_kategorien} />
				</div>
			}
			{
				_produkte.length > 0 &&
				<div>
					<h2>Produkt</h2>
					<Grid id="produkte" columns={columns} content={_produkte} />
				</div>
			}
			{
				detailsProduct &&
				<Dialog
					cancelButton="Abbrechen"
					confirmButton="Bestellen"
					content={
						<Form>
							{
								detailsProduct.beschreibung &&
								<div>{detailsProduct.beschreibung}</div>
							}
							<TextArea fluid="true" placeholder="Bemerkungen zur Bestellung" onBlur={ev => setBemerkungen(ev.target.value)} />
						</Form>
					}
					header={detailsProduct.name}
					open={detailsOpen}
					onCancel={() => setDetailsOpen(false)}
					onConfirm={() => setDetailsOpen(() => {
						if (bemerkungen && bemerkungen.length > 0) {
							setDetailsProdukt(oldItem => {
								const clone = { ...oldItem }
								clone.bemerkungen = bemerkungen
								console.log('mit bemerkungen: ', clone)
								setProdukt(clone)
							})
						}
						else {
							setDetailsProdukt(oldItem => {
								console.log('ohne bemerkungen: ', oldItem)
								setProdukt(oldItem)
							})
						}
						setDetailsProdukt(null)
						setBemerkungen(null)
						return false
					})}
				/>
			}
		</div>
	)
}

export default Karte