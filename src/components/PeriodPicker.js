import React from 'react'
import TimePicker from './TimePicker'
import { Button, Label, Flex } from '@fluentui/react'

const PeriodPicker = ({ von, bis, setVon, setBis }) => {

	return (
		<Flex gap="gap.small" wrap>
			<Label color="black" content={<strong>Zeitraum</strong>} />
			<Label color="black" content="von:" />
			<TimePicker time={von} resetValue={new Date('1970-01-01T00:00:00.000')} onChange={setVon} />
			<Label color="black" content="bis:" />
			<TimePicker time={bis} resetValue={new Date('2999-12-31T23:59:59.999')} onChange={setBis} />
			<Button onClick={() => {
				setVon(new Date((new Date().getTime() - 24 * 60 * 60 * 1000)))
				setBis(new Date())
			}}>Letzte 24 Stunden</Button>
			<Button onClick={() => {
				setVon(new Date((new Date().getTime() - 7 * 24 * 60 * 60 * 1000)))
				setBis(new Date())
			}}>Letzte 7 Tage</Button>
			<Button onClick={() => {
				setVon(new Date((new Date().getTime() - 30 * 24 * 60 * 60 * 1000)))
				setBis(new Date())
			}}>Letzte 30 Tage</Button>
		</Flex>
	)

}

export default PeriodPicker
