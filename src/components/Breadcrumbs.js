import React from 'react'

const Breadcrumbs = ({ breadcrumbs, setBreadcrumbs, cardStyle, calcColor }) => {

	const breadcrumbsCardStyle = {
		height: '3rem',
		lineHeight: '3rem'
	}

	const _breadcrumbs = breadcrumbs.map((k, index) => <div key={k.name} index={index} style={calcColor({ ...cardStyle, ...breadcrumbsCardStyle }, k.name)}
		onClick={ev => {
			setBreadcrumbs(k, breadcrumbs.slice(0, Number(ev.target.getAttribute('index')) + 1))
		}}>{k.name} &gt;</div>)

	return _breadcrumbs
}

export default Breadcrumbs