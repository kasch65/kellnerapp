import React, { useState, useEffect } from 'react'
import kartenService from './services/karte'
import mitarbeiterService from './services/mitarbeiter'
import tischeService from './services/tische'
import bestellungenService from './services/bestellungen'
import rechnungenService from './services/rechnungen'
import trinkgelderService from './services/trinkgelder'
import './App.css'
import Login from './Login'
import Mitarbeiter from './Mitarbeiter'
import WerWo from './WerWo'
import Kellnerblock from './Kellnerblock'
import Bestellungen from './Bestellungen'
import Rechnung from './Rechnung'
import Abrechnung from './Abrechnung'
import Trinkgelder from './Trinkgelder'
import * as serviceWorker from '../serviceWorker'
import { Checkbox, Form, Label, Flex } from '@fluentui/react'

const App = () => {
	const [rolleKellner, setRolleKellner] = useState(false)
	const [rolleKassierer, setRolleKassierer] = useState(false)
	const [rolleTresen, setRolleTresen] = useState(false)
	const [rolleKueche, setRolleKueche] = useState(false)
	const [rolleBuchhaltung, setRolleBuchhaltung] = useState(false)
	const [rolleChef, setRolleChef] = useState(false)

	const [karte, setKarte] = useState([])
	const [bedienungen, setMitarbeiter] = useState([])
	const [tische, setTische] = useState([])
	const [bestellungen, setBestellungen] = useState([])
	const [rechnungen, setRechnungen] = useState([])
	const [trinkgelder, setTrinkgelder] = useState([])
	const [bedienung, setBedienung] = useState('')
	const [tisch, setTisch] = useState(0)

	const API_BASE_URL = '/api/'

	useEffect(() => {
		// Request data from MongoDB
		const kartePromise = kartenService.getKarte(API_BASE_URL)
		const mitarbeiterPromise = mitarbeiterService.getBedienungen(API_BASE_URL)
		const tischePromise = tischeService.getTische(API_BASE_URL)
		Promise.all([kartePromise, mitarbeiterPromise, tischePromise]).then(values => {
			setKarte(values[0])
			setMitarbeiter(values[1])
			setBedienung(values[1][0])
			setTische(values[2])
			setTisch(values[2][0].nummer)
		})
	}, [])

	const _updateRechnungen = () => {
		const rechnungenPromise = rechnungenService.getRechnungen(API_BASE_URL)
		rechnungenPromise.then(rg => setRechnungen(rg))
		return rechnungenPromise
	}

	const _updateTrinkgelder = () => {
		const trinkgelderPromise = trinkgelderService.getTrinkgelder(API_BASE_URL)
		trinkgelderPromise.then(tr => setTrinkgelder(tr))
		return trinkgelderPromise
	}

	const _processPush = data => {
		console.log('Got push notification from service worker: ', data)
		if (data.kind === 'bestellungen') {
			if (data.action === 'post') {
				setBestellungen(oldArray => {
					const oldItem = oldArray.find(b => b.id === data.id)
					if (!oldItem) {
						return oldArray.concat(data.body)
					}
					else {
						return oldArray
					}
				})
			}
			else if (data.action === 'patch') {
				//_chefKomponentenNeuRendern()
				setBestellungen(oldArray => {
					const oldItem = oldArray.find(b => b.id === data.id)
					if (oldItem) {
						const changedEntries = Object.entries(data.body)
						changedEntries.map(e => oldItem[e[0]] = e[1])
						console.log('Updated: ', oldItem)
						return [...oldArray]
					}
					else {
						return oldArray
					}
				})
			}
			else if (data.action === 'delete') {
				setBestellungen(oldArray => oldArray.filter(b => b.id !== data.id))
			}
		}
		if (data.kind === 'mitarbeiter') {
			if (data.action === 'post') {
				_chefKomponentenNeuRendern()
				setMitarbeiter(oldArray => {
					const oldItem = oldArray.find(b => b.id === data.id)
					if (!oldItem) {
						return oldArray.concat(data.body)
					}
					else {
						return oldArray
					}
				})
			}
			else if (data.action === 'patch') {
				_chefKomponentenNeuRendern()
				setMitarbeiter(oldArray => {
					const oldItem = oldArray.find(b => b.id === data.id)
					if (oldItem) {
						const changedEntries = Object.entries(data.body)
						changedEntries.map(e => oldItem[e[0]] = e[1])
						return [...oldArray]
					}
					else {
						return oldArray
					}
				})
			}
			else if (data.action === 'delete') {
				_chefKomponentenNeuRendern()
				setMitarbeiter(old => old.filter(b => b.id !== data.id))
			}
		}
	}

	const _pushHandler = event => _processPush(event.data)

	const _login = person => {
		// Register an event listener to process push messages received by ServiceWorker

		if (!person) {
			setRolleKellner(false)
			setRolleKassierer(false)
			setRolleTresen(false)
			setRolleKueche(false)
			setRolleBuchhaltung(false)
			setRolleChef(false)
			setBedienung('')
			if ('serviceWorker' in navigator) {
				console.log('unregistering push event listener...')
				window.registration = null
				serviceWorker.unregisterPush()
				navigator.serviceWorker.removeEventListener('message', _pushHandler)
				console.log('Abgemeldet.')
			}
			return null
		}
		else {
			console.log('Login from: ', person.name)
			setRolleKellner(person.rolleBedienung === true)
			setRolleKassierer(person.rolleKassierer === true)
			setRolleTresen(person.rolleTresen === true)
			setRolleKueche(person.rolleKueche === true)
			setRolleBuchhaltung(person.rolleBuchhaltung === true)
			setRolleChef(person.rolleChef === true)
			setBedienung(person.name)
			if ('serviceWorker' in navigator) {
				console.log('registering push event listener...')
				serviceWorker.registerPush(registration => {
					window.registration = registration
					console.log('Registration: ', window.registration)
				}, window)
				navigator.serviceWorker.removeEventListener('message', _pushHandler)
				navigator.serviceWorker.addEventListener('message', _pushHandler)
			}
			else {
				console.log('No serviceworker for update push available')
			}
			bestellungenService.getBestellungen(API_BASE_URL)
				.then(b => setBestellungen(b))

			return person
		}
	}

	const _addBestellung = bestellung => bestellungenService.addBestellung(API_BASE_URL, bestellung)
		.then(b => setBestellungen(old => old.concat(b)))

	const _deleteBestellung = (del) => {
		bestellungenService.deleteBestellung(API_BASE_URL, del)
			.then(() => setBestellungen(old => old.filter(b => b.id !== del.id)))
	}

	const _setBereit = (checked, best) => {
		console.log('CB state: ', checked)
		setBestellungen(oldArray => {
			best.bereit = checked
			console.log('neuerStatus: ', best.bereit)
			const patch = {}
			patch.id = best.id
			patch.bereit = best.bereit
			bestellungenService.patchBestellung(API_BASE_URL, patch, best)
			return [...oldArray]
		})
	}

	const _setRechnung = (checked, best) => {
		console.log('CB state: ', checked)
		setBestellungen(oldArray => {
			best.rechnung = checked
			console.log('neuerStatus: ', best.rechnung)
			return [...oldArray]
		})
	}

	const _onAbgerechnet = (best, trinkgeld) => {
		// Trinkgelder und Rechnungen aktualisieren
		const primaryPromises = []
		const secPromises = []
		const bezahlteBestellungen = []
		best.filter(b => b.bezahlt).map(bezahlt => {
			const primaryPromise = rechnungenService.addRechnung(API_BASE_URL, bezahlt)
			primaryPromises.push(primaryPromise)
			primaryPromise.then(() => {
				const secPromise = bestellungenService.deleteBestellung(API_BASE_URL, bezahlt)
				secPromises.push(secPromise)
				bezahlteBestellungen.push(bezahlt)
			})
			return true
		})
		primaryPromises.push(trinkgelderService.addTrinkgeld(API_BASE_URL, trinkgeld))
		Promise.all(primaryPromises)
			.then(() => Promise.all(secPromises)
				.then(() => {
					// Refresh from server
					rechnungenService.getRechnungen(API_BASE_URL).then(r => setRechnungen(r))
					trinkgelderService.getTrinkgelder(API_BASE_URL).then(t => setTrinkgelder(t))
					// Alle besahlten Bestellungen aus Bestellungen entfernen
					setBestellungen(best.filter(b => !b.bezahlt))
				}))
	}

	const _chefKomponentenNeuRendern = () => {
		// Force recreation
		setRolleChef(vorherChef => {
			if (vorherChef) {
				setTimeout(() => setRolleChef(true), 100)
				return false
			}
			else {
				return vorherChef
			}
		})
	}

	const _saveMitarbeiterChanges = (changes, newMas) => {
		const primaryPromises = []
		const secondaryPromises = []
		changes.map(change => {
			primaryPromises.push(
				setMitarbeiter(oldMitarbeiter => {
					const clone = [...oldMitarbeiter]
					if (change.action === 'update') {
						const changedMa = clone.find(m => m.id === change.id)
						if (changedMa) {
							changedMa[change.field] = change.value
							// Send change to server
							const patch = {}
							patch.id = change.id
							patch[change.field] = change.value
							mitarbeiterService.patchBedienung(API_BASE_URL, patch)
						}
					}
					else if (change.action === 'delete') {
						const deleteBedienungReq = mitarbeiterService.deleteBedienung(API_BASE_URL, change.id)
						secondaryPromises.push(deleteBedienungReq)
						deleteBedienungReq.then(() => secondaryPromises.push(
							setMitarbeiter(old => old.filter(m => m.id !== change.id)))
						)
					}
					return clone
				})
			)
			return null
		})
		newMas.map(newMa => {
			const addBedienungReq = mitarbeiterService.addBedienung(API_BASE_URL, newMa.content)
			secondaryPromises.push(addBedienungReq)
			addBedienungReq.then(persistedMa => secondaryPromises.push(
				setMitarbeiter(old => old.concat(persistedMa))
			))
			return null
		})
		Promise.all(primaryPromises).then(() => Promise.all(secondaryPromises).then(() => _chefKomponentenNeuRendern()))
	}

	return (
		<div id="app">
			<Login bedienungen={bedienungen} onLogin={person => _login(person)} />
			{
				bedienung && bedienung.length >= 0 &&
				<Form id="rollen">
					<Flex gap="gap.small" wrap>
						<Label color="black" content={<strong>Rollen</strong>} />
						<Checkbox label="Kellner" checked={rolleKellner} onClick={() => setRolleKellner(oldValue => !oldValue)} />
						<Checkbox label="Kassierer" checked={rolleKassierer} onClick={() => setRolleKassierer(oldValue => !oldValue)} />
						<Checkbox label="Tresen" checked={rolleTresen} onClick={() => setRolleTresen(oldValue => !oldValue)} />
						<Checkbox label="Küche" checked={rolleKueche} onClick={() => setRolleKueche(oldValue => !oldValue)} />
						<Checkbox label="Buchhaltung" checked={rolleBuchhaltung} onClick={() => setRolleBuchhaltung(oldValue => !oldValue)} />
						<Checkbox label="Chef" checked={rolleChef} onClick={() => setRolleChef(oldValue => !oldValue)} />
					</Flex>
				</Form>
			}
			{
				(rolleChef) &&
				<Mitarbeiter bedienungen={bedienungen} saveMitarbeiterChanges={_saveMitarbeiterChanges} />
			}
			{
				(rolleKellner || rolleKassierer || rolleBuchhaltung || rolleChef) &&
				<WerWo bedienungen={bedienungen} tische={tische} bedienung={bedienung} tisch={tisch} setBedienung={setBedienung} setTisch={setTisch} />
			}
			{
				(rolleKellner || rolleChef) &&
				<Kellnerblock karte={karte} bedienung={bedienung} tisch={tisch} addBestellung={_addBestellung} />
			}
			{
				(rolleKellner || rolleKassierer || rolleTresen || rolleKueche || rolleChef) &&
				<Bestellungen bestellungen={bestellungen} bedienung={bedienung} tisch={tisch} setBestellungen={setBestellungen} setBereit={_setBereit} setRechnung={_setRechnung} onDelete={_deleteBestellung} rollen={{ rolleKellner, rolleKassierer, rolleTresen, rolleKueche, rolleBuchhaltung, rolleChef }} />
			}
			{
				(rolleKassierer || rolleChef) &&
				<Rechnung bestellungen={bestellungen} bedienung={bedienung} tisch={tisch} setRechnung={_setRechnung} onAbgerechnet={_onAbgerechnet} />
			}
			{
				(rolleKassierer || rolleBuchhaltung || rolleChef) &&
				<Abrechnung rechnungen={rechnungen} bedienung={bedienung} tisch={tisch} rollen={{ rolleKellner, rolleKassierer, rolleTresen, rolleKueche, rolleBuchhaltung, rolleChef }} updateRechnungen={_updateRechnungen} />
			}
			{
				(rolleKellner || rolleKassierer || rolleTresen || rolleBuchhaltung || rolleChef) &&
				<Trinkgelder trinkgelder={trinkgelder} bedienung={bedienung} tisch={tisch} rollen={{ rolleKellner, rolleKassierer, rolleTresen, rolleKueche, rolleBuchhaltung, rolleChef }} updateTrinkgelder={_updateTrinkgelder} />
			}
		</div>
	)

}

export default App
