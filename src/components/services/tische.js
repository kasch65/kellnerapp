import axios from 'axios'

const getTische = apiBaseUrl => {
	return axios.get(apiBaseUrl + 'tische').then(response => response.data)
}

export default { getTische }
