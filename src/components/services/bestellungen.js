import axios from 'axios'

const getBestellungen = apiBaseUrl => {
	return axios.get(apiBaseUrl + 'bestellungen').then(response => response.data)
}

const addBestellung = (apiBaseUrl, bestellung) => {
	const clone = { ...bestellung }
	delete (clone.id)
	delete (clone.id)
	if ('registration' in window) {
		clone.originEndpoint = window.registration.endpoint
	}
	return axios.post(apiBaseUrl + 'bestellungen', clone).then(response => response.data)
}

const patchBestellung = (apiBaseUrl, bestellungPatch, details) => {
	const id = bestellungPatch.id
	delete (bestellungPatch.id)
	const headers = {
		headers: {}
	}
	if ('registration' in window) {
		headers.headers.originEndpoint = window.registration.endpoint
	}
	const body = {
		patch: bestellungPatch,
		details: details
	}
	return axios.patch(`${apiBaseUrl}bestellungen/${id}`, body, headers)
		.then(response => response.data)
}

const deleteBestellung = (apiBaseUrl, bestellung) => {
	const headers = {
		headers: {}
	}
	if ('registration' in window) {
		headers.headers.originEndpoint = window.registration.endpoint
	}
	return axios.delete(`${apiBaseUrl}bestellungen/${bestellung.id}`, headers)
		.then(response => response.data)
		.catch(err => {
			if (err.response.status === 404) {
				console.warn('Local orders out of date. Please re login!: ', err.response)
				alert('Bestellungen nicht aktuell. Bitte abmelden und erneut anmelden!')
				return bestellung
			}
			else {
				throw err
			}
		})
}

export default { getBestellungen, addBestellung, patchBestellung, deleteBestellung }
