import axios from 'axios'

const getTrinkgelder = apiBaseUrl => {
	return axios.get(apiBaseUrl + 'trinkgelder').then(response => response.data)
}

const addTrinkgeld = (apiBaseUrl, trinkgeld) => {
	const clone = { ...trinkgeld }
	delete (clone.id)
	delete (clone.id)
	return axios.post(apiBaseUrl + 'trinkgelder', clone).then(response => response.data)
}

export default { getTrinkgelder, addTrinkgeld }
