import axios from 'axios'

const getKarte = apiBaseUrl => {
	return axios.get(apiBaseUrl + 'karte').then(response => response.data[0])
}

export default { getKarte }
