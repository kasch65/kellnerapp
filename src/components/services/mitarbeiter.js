import axios from 'axios'

const getBedienungen = apiBaseUrl => {
	return axios.get(`${apiBaseUrl}mitarbeiter`).then(response => response.data)
}

const addBedienung = (apiBaseUrl, mitarbeiter) => {
	const clone = { ...mitarbeiter }
	delete (clone.id)
	delete (clone.id)
	delete (clone.tmpId)
	if ('registration' in window) {
		clone.originEndpoint = window.registration.endpoint
	}
	return axios.post(apiBaseUrl + 'mitarbeiter', clone).then(response => response.data)
}

const patchBedienung = (apiBaseUrl, mitarbeiterPatch) => {
	const id = mitarbeiterPatch.id
	delete (mitarbeiterPatch.id)
	const headers = {
		headers: {}
	}
	if ('registration' in window) {
		headers.headers.originEndpoint = window.registration.endpoint
	}
	return axios.patch(`${apiBaseUrl}mitarbeiter/${id}`, mitarbeiterPatch, headers)
		.then(response => response.data)
}

const deleteBedienung = (apiBaseUrl, id) => {
	const headers = {
		headers: {}
	}
	if ('registration' in window) {
		headers.headers.originEndpoint = window.registration.endpoint
	}
	return axios.delete(`${apiBaseUrl}mitarbeiter/${id}`, headers)
		.then(response => response.data)
}

export default { getBedienungen, addBedienung, patchBedienung, deleteBedienung }
