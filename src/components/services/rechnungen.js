import axios from 'axios'

const getRechnungen = apiBaseUrl => {
	return axios.get(apiBaseUrl + 'rechnungen').then(response => response.data)
}

const addRechnung = (apiBaseUrl, bestellung) => {
	const clone = { ...bestellung }
	delete (clone.id)
	delete (clone.id)
	return axios.post(apiBaseUrl + 'rechnungen', clone).then(response => response.data)
}

export default { getRechnungen, addRechnung }
