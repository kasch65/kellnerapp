import React, { useState } from 'react'
import { Label, Button, Form, Input, Flex } from '@fluentui/react'

const Login = ({ bedienungen, onLogin }) => {
	const [pin, setPin] = useState('')
	const [loggedIn, setLoggedIn] = useState(false)
	const [person, setPerson] = useState(null)

	const longerScreenSize = Math.max(window.innerWidth, window.innerHeight)

	const bgUrls = [
		[493, 'nils-stahl-BCkLxilDvJU-unsplash.740x493.jpg'],
		[541, 'nils-stahl-BCkLxilDvJU-unsplash.812x541.jpg'],
		[768, 'nils-stahl-BCkLxilDvJU-unsplash.1152x768.jpg'],
		[1024, 'nils-stahl-BCkLxilDvJU-unsplash.1536x1024.jpg'],
		[1280, 'nils-stahl-BCkLxilDvJU-unsplash.1920x1280.jpg'],
		[300000, 'nils-stahl-BCkLxilDvJU-unsplash.jpg']
	]

	const bgUrl = bgUrls.find(url => url[0] >= longerScreenSize)[1]

	console.log(`Bg for image size ${longerScreenSize} is ${bgUrl}`)

	const loginStyle = {
		width: '100vw',
		height: '100vh',
		backgroundImage: `url("${bgUrl}")`,
		backgroundSize: 'cover',
		backgroundPosition: 'center'
	}

	const loginInputStyle = {
		margin: '5rem auto'
	}

	return (
		<Form id="login">
			{
				!loggedIn &&
				<div style={loginStyle}>
					<Input type="password" placeholder="Anmelde-PIN" aria-label="Anmelde-PIN" value={pin} onChange={(event) => {
						const newPin = event.target.value
						setPin(newPin)
						if (!newPin || newPin.length !== 4) {
							return
						}
						const newPerson = onLogin(bedienungen.find(b => b.pin === newPin))
						if (newPerson) {
							setLoggedIn(true)
							setPerson(newPerson)
						}
					}} styles={{
						padding: '.5rem',
					}} style={loginInputStyle}/>
				</div>
			}
			{
				loggedIn &&
				<div>
					<Flex gap="gap.small" wrap>
						<Label color="black" content={<strong>{person.name}</strong>} />
						<Button primary aria-label="Abmelden" icon="lock" iconOnly title="Abmelden" onClick={() => {
							onLogin(null)
							setPin('')
							setLoggedIn(false)
						}} />
					</Flex>
				</div>
			}
		</Form>
	)
}

export default Login