import React, { useState } from 'react'
import Table from './Table'
import PeriodPicker from './PeriodPicker'
import { Checkbox, Button, Flex } from '@fluentui/react'

const Trinkgelder = ({ trinkgelder, bedienung, tisch, rollen, updateTrinkgelder }) => {
	const [alleTische, setAlleTische] = useState(true)
	const [alleKassierer, setAlleKassierer] = useState(rollen.rolleBuchhaltung || rollen.rolleChef)
	const [von, setVon] = useState(new Date((new Date().getTime() - 24 * 60 * 60 * 1000)))
	const [bis, setBis] = useState(new Date('2999-12-31T23:59:59.999'))
	const [expanded, setExpanded] = useState(false)
	const [loading, setLoading] = useState(false)

	const clickStyle = {
		cursor: 'pointer'
	}

	const _tableHeaders = ['Datum', 'Tisch', 'Kassierer', 'Trinkgeld']

	const _tableContent = trinkgelder
		.filter(b => new Date(b.rechnungsdatum).getTime() >= von.getTime())
		.filter(b => new Date(b.rechnungsdatum).getTime() < bis.getTime())
		.filter(b => b.tisch === tisch || alleTische)
		.filter(b => b.kassierer === bedienung || alleKassierer)
		.sort((a, b) => new Date(b.rechnungsdatum).getTime() - new Date(a.rechnungsdatum).getTime())
		.map((b) =>
			[
				['datum', new Date(b.rechnungsdatum).toLocaleString()],
				['tisch', b.tisch],
				['kassierer', b.kassierer],
				['trinkgeld', b.trinkgeld.toFixed(2)],
			]
		)

	_tableContent.push([
		['datum', ''],
		['tisch', ''],
		// eslint-disable-next-line react/jsx-key
		['kassierer', <strong>Summe:</strong>],
		// eslint-disable-next-line react/jsx-key
		['trinkgeld', <strong>{
			trinkgelder
				.filter(b => new Date(b.rechnungsdatum).getTime() >= von.getTime())
				.filter(b => new Date(b.rechnungsdatum).getTime() < bis.getTime())
				.filter(b => b.tisch === tisch || alleTische)
				.filter(b => b.kassierer === bedienung || alleKassierer)
				.reduce((summe, b) => summe += b.trinkgeld, 0).toFixed(2)
		}</strong>
		]
	])

	return (
		<div id="trinkgelder">
			<h1 onClick={() => {
				if (!expanded) {
					setLoading(true)
					updateTrinkgelder()
						.then(() => {
							setExpanded(oldValue => !oldValue)
							setLoading(false)
						})
				}
				else {
					setExpanded(oldValue => !oldValue)
				}
			}} style={clickStyle}>Trinkgelder <Button circular icon={expanded ? 'chevron-down' : 'icon-chevron-end'} iconOnly loader="expand" loading={loading} /></h1>
			{
				expanded &&
				<>
					<Flex gap="gap.small" wrap>
						<Checkbox label="Alle Kassierer" checked={alleKassierer} onClick={() => setAlleKassierer(oldValue => !oldValue)} />
						<Checkbox label="Alle Tische" checked={alleTische} onClick={() => setAlleTische(oldValue => !oldValue)} />
					</Flex>
					<PeriodPicker von={von} bis={bis} setVon={setVon} setBis={setBis} />

					<Table tableHeaders={_tableHeaders} tableContent={_tableContent} getRowKey={row => row[0][1]} />
				</>
			}
		</div>
	)

}

export default Trinkgelder
