import React, { useState } from 'react'
import Karte from './Karte'
import { Button } from '@fluentui/react'

const Kellnerblock = ({ karte, bedienung, tisch, addBestellung }) => {
	const [expanded, setExpanded] = useState(true)

	const _onOrder = (produkt) => {
		const produktClone = { ...produkt }
		produktClone.bedienung = bedienung
		produktClone.tisch = tisch
		produktClone.bestelldatum = new Date()
		addBestellung(produktClone)
	}

	return (
		<div id="kellnerBlock">
			<h1>Block <Button circular icon={expanded ? 'chevron-down' : 'icon-chevron-end'} iconOnly loader="expand" onClick={() => setExpanded(oldValue => !oldValue)} /></h1>
			{
				expanded &&
				<Karte karte={karte} setProdukt={(produkt) => _onOrder(produkt)} />
			}
		</div>
	)
}

export default Kellnerblock