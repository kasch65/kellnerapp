import React, { useState } from 'react'
import Table from './Table'
import { Button, Checkbox, Form, Input } from '@fluentui/react'

const Rechnung = ({ bestellungen, bedienung, tisch, setRechnung, onAbgerechnet }) => {
	const [bitte, setBitte] = useState('0.00')
	const [gegeben, setGegeben] = useState('0.00')
	const [rueckgeld, setRueckgeld] = useState('0.00')
	const [trinkgeld, setTrinkgeld] = useState(bestellungen.filter(b => b.rechnung).reduce((summe, b) => summe -= b.preis, 0).toFixed(2))
	const [expanded, setExpanded] = useState(true)

	const clickStyle = {
		cursor: 'pointer'
	}

	let summe = bestellungen.filter(b => b.rechnung).reduce((summe, b) => summe += b.preis, 0)
	let timeout

	const _bitteVerarbeiten = () => {
		setBitte(raw => {
			const bitteNumber = Number(raw)
			const gegebenNumber = Number(gegeben)
			if (bitteNumber > 0) {
				setTrinkgeld((bitteNumber - summe).toFixed(2))
				if (gegebenNumber > 0) {
					setRueckgeld((gegebenNumber - Math.max(summe, bitteNumber)).toFixed(2))
				}
			}
			return bitteNumber.toFixed(2)
		})
	}

	const _GegebenVerarbeiten = () => {
		setGegeben(raw => {
			const gegebenNumber = Number(raw)
			const bitteNumber = Number(bitte)
			if (gegebenNumber > 0) {
				if (bitteNumber > 0) {
					setRueckgeld((gegebenNumber - Math.max(summe, bitteNumber)).toFixed(2))
					setTrinkgeld((bitteNumber - summe).toFixed(2))
				}
				else {
					setRueckgeld((gegebenNumber - summe).toFixed(2))
					setTrinkgeld('0.00')
				}
			}
			return gegebenNumber.toFixed(2)
		})
	}

	const _RueckgeldVerarbeiten = () => {
		setRueckgeld(raw => {
			const rueckgeldNumber = Number(raw)
			const gegebenNumber = Number(gegeben)
			if (rueckgeldNumber > 0) {
				if (gegebenNumber > 0) {
					setTrinkgeld((gegebenNumber - rueckgeldNumber - summe).toFixed(2))
					setBitte((gegebenNumber - rueckgeldNumber).toFixed(2))
				}
			}
			return rueckgeldNumber.toFixed(2)
		})
	}

	const _tableHeaders = ['Bedienung', 'Tisch', 'Produkt', 'Preis', 'Rechnung']

	const _tableContent = bestellungen.filter(b => b.rechnung).map((b) =>
		[
			['bedienung', b.bedienung],
			['tisch', b.tisch],
			['name', b.name],
			['preis', b.preis.toFixed(2)],
			['rechnung', <Checkbox key="rechnung" checked={b.rechnung} onClick={() => setRechnung(!b.rechnung, b)} />],
		]
	)

	return (
		<>
			{
				bestellungen.filter(b => b.rechnung).length > 0 &&
				<Form id="rechnungen">
					<h1 onClick={() => setExpanded(oldValue => !oldValue)} style={clickStyle}>Rechnung <Button circular icon={expanded ? 'chevron-down' : 'icon-chevron-end'} iconOnly loader="expand" /></h1>
					{
						expanded &&
						<>
							<Table tableHeaders={_tableHeaders} tableContent={_tableContent} />

							<table>
								<tbody>
									<tr>
										<td>Summe:</td>
										<td><Input placeholder={summe.toFixed(2)} disabled /></td>
									</tr>
									<tr>
										<td>Bitte:</td>
										<td>
											<Input
												value={bitte}
												onChange={ev => {
													setBitte(ev.target.value)
													if (!isNaN(ev.target.value)) {
														if (timeout) {
															clearTimeout(timeout)
														}
														timeout = setTimeout(() => _bitteVerarbeiten(), 2000)
													}
												}}
												onBlur={() => _bitteVerarbeiten()}
												onFocus={ev => ev.target.select()}
											/>
										</td>
									</tr>
									<tr>
										<td>Gegeben:</td>
										<td>
											<Input
												value={gegeben}
												onChange={ev => {
													setGegeben(ev.target.value)
													if (!isNaN(ev.target.value)) {
														if (timeout) {
															clearTimeout(timeout)
														}
														timeout = setTimeout(() => _GegebenVerarbeiten(), 2000)
													}
												}}
												onBlur={() => _GegebenVerarbeiten()}
												onFocus={ev => ev.target.select()}
											/>
										</td>
									</tr>
									<tr>
										<td>Rückgeld:</td>
										<td>
											<Input
												value={rueckgeld}
												onChange={ev => {
													setRueckgeld(ev.target.value)
													if (!isNaN(ev.target.value)) {
														if (timeout) {
															clearTimeout(timeout)
														}
														timeout = setTimeout(() => _RueckgeldVerarbeiten(), 2000)
													}
												}}
												onBlur={() => _RueckgeldVerarbeiten()}
												onFocus={ev => ev.target.select()}
											/>
										</td>
									</tr>
									<tr>
										<td>Trinkgeld:</td>
										<td><Input placeholder={trinkgeld} disabled /></td>
									</tr>
								</tbody>
							</table>
							<Button content="Fertig" icon="accept" iconPosition="after" loader="Fertig" primary onClick={() => {
								const trinkgeldNumber = Number(trinkgeld)
								const rechnungsdatum = new Date()
								const clone = [...bestellungen]
								clone.filter(b => b.rechnung).map(b => {
									b.bezahlt = true
									b.rechnungsdatum = rechnungsdatum
									b.kassierer = bedienung
									return null
								})
								onAbgerechnet(clone, { rechnungsdatum, kassierer: bedienung, tisch, trinkgeld: trinkgeldNumber })
								summe = 0
								setBitte('0.00')
								setGegeben('0.00')
								setRueckgeld('0.00')
								setTrinkgeld('0.00')
							}} />
						</>
					}
				</Form>
			}
		</>
	)

}

export default Rechnung
