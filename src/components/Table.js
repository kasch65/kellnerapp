import React from 'react'

const Table = ({ tableHeaders, tableContent, getRowKey }) => {

	const tableStyle = {
		maxWidth: '100vw'
	}

	const rowStyle = {
		borderBottom: 'solid #333 1px'
	}

	const cellStyle = {
		height: '3rem',
		borderBottom: 'solid #333 1px'
	}

	return (
		<table style={tableStyle}>
			<thead style={rowStyle}>
				<tr>
					{
						tableHeaders.map(h => <th key={h} style={cellStyle}>{h}</th>)
					}
				</tr>
			</thead>
			<tbody>
				{
					tableContent.map(row => <tr key={getRowKey ? getRowKey(row) : row[2][1]} style={rowStyle}>{
						row.map(col => <td key={col[0]} style={cellStyle}>{col[1]}</td>)
					}</tr>)
				}
			</tbody>
		</table>
	)
}

export default Table