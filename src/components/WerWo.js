import React from 'react'
import { Form, Label, Dropdown, Flex } from '@fluentui/react'

const WerWo = ({ bedienungen, tische, tisch, bedienung, setBedienung, setTisch }) => {

	return (
		<Form id="werWo">
			<Flex gap="gap.medium" wrap>
				<Label color="black" content={<strong>Filter</strong>} />
				<Label color="black" content="Kellner:" />
				<Dropdown inline={true} label="Kellner" items={bedienungen.map(b => b.name)} value={bedienung} checkable getA11ySelectionMessage={{
					onAdd: item => setBedienung(item)
				}} />
				<Label color="black" content="Tisch:" />
				<Dropdown inline={true} label="Tisch" items={tische.map(t => t.nummer)} value={tisch} checkable getA11ySelectionMessage={{
					onAdd: item => setTisch(item)
				}} />
			</Flex>
		</Form>
	)
}

export default WerWo