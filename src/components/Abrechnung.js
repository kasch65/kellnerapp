import React, { useState } from 'react'
import PeriodPicker from './PeriodPicker'
import Table from './Table'
import { Checkbox, Button, Form, Flex } from '@fluentui/react'

const Abrechnung = ({ rechnungen, bedienung, tisch, rollen, updateRechnungen }) => {
	const [alleTische, setAlleTische] = useState(rollen.rolleKassierer || rollen.rolleBuchhaltung || rollen.rolleChef)
	const [alleBedienungen, setAlleBedienungen] = useState(true)
	const [alleKassierer, setAlleKassierer] = useState(rollen.rolleBuchhaltung || rollen.rolleChef)
	const [von, setVon] = useState(new Date((new Date().getTime() - 24 * 60 * 60 * 1000)))
	const [bis, setBis] = useState(new Date('2999-12-31T23:59:59.999'))
	const [expanded, setExpanded] = useState(false)
	const [loading, setLoading] = useState(false)

	const formStyle = {
		display: 'block'
	}

	const clickStyle = {
		cursor: 'pointer'
	}

	/**
	 * See: https://gist.github.com/JamieMason/0566f8412af9fe6a1d470aa1e089a752
	 * @param {*} key Name of the gruping field as string
	 */
	/*const groupBy = key => array =>
		array.reduce((objectsByKeyValue, obj) => {
			const value = obj[key];
			objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
			return objectsByKeyValue;
		}, {});*/

	const _tableHeaders = ['Datum', 'Tisch', 'Kellner', 'Kassierer', 'Produkt', 'Preis']

	const _tableContent = rechnungen
		.filter(b => new Date(b.rechnungsdatum).getTime() >= von.getTime())
		.filter(b => new Date(b.rechnungsdatum).getTime() < bis.getTime())
		.filter(b => b.tisch === tisch || alleTische)
		.filter(b => b.kassierer === bedienung || alleKassierer)
		.filter(b => b.bedienung === bedienung || alleBedienungen)
		.map((b) =>
			[
				['datum', new Date(b.rechnungsdatum).toLocaleString(), b.id],
				['tisch', b.tisch],
				['kellner', b.bedienung],
				['kassierer', b.kassierer],
				['name', b.name],
				['preis', b.preis.toFixed(2)],
			]
		)

	_tableContent.push([
		['datum', '', Math.random()],
		['tisch', ''],
		['kellner', ''],
		['kassierer', ''],
		// eslint-disable-next-line react/jsx-key
		['name', <strong>Summe:</strong>],
		// eslint-disable-next-line react/jsx-key
		['preis', <strong>{
			rechnungen
				.filter(b => new Date(b.rechnungsdatum).getTime() >= von.getTime())
				.filter(b => new Date(b.rechnungsdatum).getTime() < bis.getTime())
				.filter(b => b.tisch === tisch || alleTische)
				.filter(b => b.kassierer === bedienung || alleKassierer)
				.filter(b => b.bedienung === bedienung || alleBedienungen)
				.sort((a, b) => new Date(a.rechnungsdatum).getTime() - new Date(b.rechnungsdatum).getTime())
				.reduce((summe, b) => summe += b.preis, 0).toFixed(2)
		}</strong>
		]
	])

	return (
		<Form id="abrechnung" style={formStyle}>
			<div>
				<h1 onClick={() => {
					if (!expanded) {
						setLoading(true)
						updateRechnungen()
							.then(() => {
								setExpanded(oldValue => !oldValue)
								setLoading(false)
							})
					}
					else {
						setExpanded(oldValue => !oldValue)
					}
				}} style={clickStyle}>Abrechnung <Button circular icon={expanded ? 'chevron-down' : 'icon-chevron-end'} iconOnly loader="expand" loading={loading} /></h1>
				{
					expanded &&
					<>
						<Flex gap="gap.small" wrap>
							<Checkbox label="Alle Bedienungen" checked={alleBedienungen} onClick={() => setAlleBedienungen(oldValue => !oldValue)} />
							<Checkbox label="Alle Kassierer" checked={alleKassierer} onClick={() => setAlleKassierer(oldValue => !oldValue)} />
							<Checkbox label="Alle Tische" checked={alleTische} onClick={() => setAlleTische(oldValue => !oldValue)} />
						</Flex>
						<PeriodPicker von={von} bis={bis} setVon={setVon} setBis={setBis} />

						<Table tableHeaders={_tableHeaders} tableContent={_tableContent} getRowKey={row => row[0][2]} />
					</>
				}
			</div>
		</Form>
	)

}

export default Abrechnung
