let isSubscribed = false
const applicationKeyDev = 'BBKcno2o-sA_vfxb3TO0VUrt6BL6OwOEmYSSUNxKLlzakx_8CjnKyOhdtt9q12n-05UXfFv-nweIHE5gBsbc4pg'
const applicationKeyProd = 'BGswTNm7yqilTp6mTbaBtTr3buL-73Jb5pt48fOLMXIJqWEVIU1fkwMBfIP1yRj63C8uoHP3Lj08YJMihfoQ-Mo'
let applicationKey

// Url Encryption
const urlB64ToUint8Array = base64String => {
	const padding = '='.repeat((4 - base64String.length % 4) % 4)
	const base64 = (base64String + padding)
		.replace(/-/g, '+')
		.replace(/_/g, '/')

	const rawData = window.atob(base64)
	const outputArray = new Uint8Array(rawData.length)

	for (let i = 0; i < rawData.length; ++i) {
		outputArray[i] = rawData.charCodeAt(i)
	}
	return outputArray
}

// Send request to sav subscription in MongoDB
let subscritionId
const saveSubscription = subscription => {
	/*axios.post('/subscribe', subscription)
		.then(response => console.log('User subscribed to server'))*/

	let xmlHttp = new XMLHttpRequest()
	xmlHttp.open('POST', '/api/subscribe')
	xmlHttp.setRequestHeader('Content-Type', 'application/json; charset=UTF-8')
	xmlHttp.onreadystatechange = () => {
		if (xmlHttp.readyState !== 4) return
		if (xmlHttp.status !== 200 && xmlHttp.status !== 304) {
			console.warn('HTTP error ' + xmlHttp.status, null)
		} else {
			console.log('User subscribed to server')
			console.log('Subscription save response is: ', xmlHttp.response)
			subscritionId = JSON.parse(xmlHttp.response).id
			console.log('Subscription id is: ', subscritionId)
		}
	}
	xmlHttp.send(JSON.stringify(subscription))
}

export const deleteSubscription = () => {
	if (subscritionId) {
		let xmlHttp = new XMLHttpRequest()
		xmlHttp.open('DELETE', `/api/subscribe/${subscritionId}`)
		xmlHttp.onreadystatechange = () => {
			if (xmlHttp.readyState !== 4) return
			if (xmlHttp.status === 204) {
				console.log('User unsubscribed from server')
				subscritionId = null
			} else {
				console.warn('HTTP error ', xmlHttp.status)
			}
		}
		xmlHttp.send()
	}
	else {
		console.log('No subscription id known.')
	}
}

// This optional code is used to register a service worker.
// register() is not called by default.

// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on subsequent visits to a page, after all the
// existing tabs open on the page have been closed, since previously cached
// resources are updated in the background.

// To learn more about the benefits of this model and instructions on how to
// opt-in, read https://bit.ly/CRA-PWA

const isLocalhost = Boolean(
	window.location.hostname === 'localhost' ||
	// [::1] is the IPv6 localhost address.
	window.location.hostname === '[::1]' ||
	// 127.0.0.1/8 is considered localhost for IPv4.
	window.location.hostname.match(
		/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
	)
)

console.log('isLocalhost', isLocalhost)

export function register(config) {
	if ('serviceWorker' in navigator) {
		console.log('Service Worker supported')
		if (process.env.NODE_ENV === 'production') {
			console.log('We´re in production environment.')
			applicationKey = applicationKeyProd
			// The URL constructor is available in all browsers that support SW.
			const publicUrl = new URL(process.env.PUBLIC_URL, window.location.href)
			if (publicUrl.origin !== window.location.origin) {
				// Our service worker won't work if PUBLIC_URL is on a different origin
				// from what our page is served on. This might happen if a CDN is used to
				// serve assets see https://github.com/facebook/create-react-app/issues/2374
				return
			}

			window.addEventListener('load', () => {
				const swUrl = `${process.env.PUBLIC_URL}/custom-service-worker.js`

				if (isLocalhost) {
					// This is running on localhost. Let's check if a service worker still exists or not.
					checkValidServiceWorker(swUrl, config)

					// Add some additional logging to localhost, pointing developers to the
					// service worker/PWA documentation.
					navigator.serviceWorker.ready.then(() => {
						console.log(
							'This web app is being served cache-first by a service ' +
							'worker. To learn more, visit https://bit.ly/CRA-PWA'
						)
					})
				} else {
					// Is not localhost. Just register service worker
					registerValidSW(swUrl, config)
				}
			})
		}
		else {
			console.log('We´re in development environment.')
			applicationKey = applicationKeyDev
			const swUrl = `${process.env.PUBLIC_URL}/custom-service-worker.js`

			if (isLocalhost) {
				// This is running on localhost. Let's check if a service worker still exists or not.
				checkValidServiceWorker(swUrl, config)

				// Add some additional logging to localhost, pointing developers to the
				// service worker/PWA documentation.
				navigator.serviceWorker.ready.then(() => {
					console.log(
						'This web app is being served cache-first by a service ' +
						'worker. To learn more, visit https://bit.ly/CRA-PWA'
					)
				})
			} else {
				// Is not localhost. Just register service worker
				registerValidSW(swUrl, config)
			}
		}
	}
}

const pushEventListener = event => {
	console.debug('Got reply from service worker: ', event.data)
}

function registerValidSW(swUrl, config) {
	navigator.serviceWorker.register(swUrl)
		.then(registration => {
			console.log('service worker registered')
			registration.onupdatefound = () => {
				const installingWorker = registration.installing
				if (installingWorker == null) {
					return
				}
				installingWorker.onstatechange = () => {
					if (installingWorker.state === 'installed') {
						if (navigator.serviceWorker.controller) {
							// At this point, the updated precached content has been fetched,
							// but the previous service worker will still serve the older
							// content until all client tabs are closed.
							console.log(
								'New content is available and will be used when all ' +
								'tabs for this page are closed. See https://bit.ly/CRA-PWA.'
							)

							// Execute callback
							if (config && config.onUpdate) {
								config.onUpdate(registration)
							}
						} else {
							// At this point, everything has been precached.
							// It's the perfect time to display a
							// "Content is cached for offline use." message.
							console.log('Content is cached for offline use.')

							// Execute callback
							if (config && config.onSuccess) {
								config.onSuccess(registration)
							}
						}
					}
				}
			}
			// Register an event listener to process push messages received by ServiceWorker
			console.log('registering push event listener...')
			navigator.serviceWorker.addEventListener('message', pushEventListener)
		})
		.catch(error => {
			console.error('Error during service worker registration:', error)
		})
}

export const registerPush = (registrationCallback) => {
	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.ready.then(registration => {
			registration.pushManager.getSubscription()
				.then(subscription => {
					isSubscribed = !(subscription === null)

					if (isSubscribed) {
						console.log('User already subscribed', subscription)
						registrationCallback(subscription)
					} else {
						registration.pushManager.subscribe({
							userVisibleOnly: true,
							applicationServerKey: urlB64ToUint8Array(applicationKey)
						})
							.then(subscription => {
								isSubscribed = true
								console.log('User has subscribed', subscription)
								if (registrationCallback) {
									registrationCallback(subscription)
								}
								// Notify server
								saveSubscription(subscription)
							})
							.catch(err => {
								console.warn('Failed to subscribe user: ', err)
							})
					}
				})
		})
	}
}

export const unregisterPush = () => {
	// Notify server
	deleteSubscription()

	if ('serviceWorker' in navigator) {
		// Register an event listener to process push messages received by ServiceWorker
		console.log('unregistering push event listener...')
		navigator.serviceWorker.removeEventListener('message', pushEventListener)

		navigator.serviceWorker.ready.then(registration => {
			registration.pushManager.getSubscription()
				.then(subscription => {
					isSubscribed = !(subscription === null)

					if (!isSubscribed) {
						console.log('User not subscribed', subscription)
					} else {
						subscription.unsubscribe()
							.then(subscription => {
								isSubscribed = false
								console.log('User has unsubscribed?', subscription)
							})
							.catch(err => {
								console.warn('Failed to unsubscribe user: ', err)
							})
					}
				})
		})
	}
}

function checkValidServiceWorker(swUrl, config) {
	// Check if the service worker can be found. If it can't reload the page.
	fetch(swUrl)
		.then(response => {
			// Ensure service worker exists, and that we really are getting a JS file.
			const contentType = response.headers.get('content-type')
			if (
				response.status === 404 ||
				(contentType != null && contentType.indexOf('javascript') === -1)
			) {
				// No service worker found. Probably a different app. Reload the page.
				navigator.serviceWorker.ready.then(registration => {
					registration.unregister().then(() => {
						window.location.reload()
					})
				})
			} else {
				// Service worker found. Proceed as normal.
				registerValidSW(swUrl, config)
			}
		})
		.catch(() => {
			console.log(
				'No internet connection found. App is running in offline mode.'
			)
		})
}

export function unregister() {
	if ('serviceWorker' in navigator) {
		navigator.serviceWorker.ready.then(registration => {
			registration.unregister()
		})
	}
}
